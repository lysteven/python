import folium
import os
import webbrowser

# Map method of folium return Map object
# Here we pass coordinates of Gfg
# and starting Zoom level = 12
my_map = folium.Map(location=[40.521986, -73.990436],  zoom_start=7)
url = 'destroyer.png'
uss_names = ['USS Chafee DDG-90	Arleigh Burke', 'USS Fort Worth	LCS-3 Freedom ',
             'USS Ashland LSD-48 ', 'USS Coronado LCS-4', 'USS Farragut	DDG-99	Arleigh Burke']
lats = [39.758280, 41.731993, 37.931182, 38.735514, 39.805212]
lons = [-70.884496, -68.997094, -68.066867, -72.602980, -73.661208]


def uss_deploy():
    for n in range(len(lats)):
        folium.Marker([lats[n], lons[n]], icon=folium.features.CustomIcon(url, icon_size=(50, 50)), popup=uss_names[n]+" Latitude:"+str(lats[n])+" Longitude:"+str(lons[n])).add_to(my_map)


def change_coordinates(la, lo, code):
    for i in range(len(lats)):
        if float(lats[code]) == la and float(lons[code]) == lo:
            print("Destroyer verified!")
            nx = input("Re-latitude:")
            nl = input("Re-longitude:")
            lats[code] = float(nx)
            lons[code] = float(nl)
            break
        else:
            print("Invalid Coordinates!")
            break


if __name__ == '__main__':
    print(lats)
    print(lons)
    xl = input("Enter lat to re-coordinate:")
    yl = input("Enter lon to re-coordinate:")
    c = input("Enter destroyer code:")
    change_coordinates(float(xl), float(yl), int(c))
    print("Latitude updated: " + str(lats))
    print("Longitude updated: " + str(lons))
    uss_deploy()
    CWD = os.getcwd()
    my_map.save("my_map.html ")
    webbrowser.open_new('file://'+CWD+'/'+'my_map.html')




# Add any directories, files, or patterns you don't want to be tracked by version control
from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
from google.cloud import translate
from tkinter.filedialog import askopenfilename
import logging

# Instantiates a client
translate_client = translate.Client()


def translate_text(*args):
    # Translates some text into another language
    enter = text.get()
    target = var.get()
    translation = translate_client.translate(enter, target_language=target)
    result.set(u'Translation: {}'.format(translation['translatedText']))


def open_file():
    name = askopenfilename(initialdir="C:/Users/Surface4/Desktop",
                           filetypes =(("Text File", "*.txt"), ("All Files", "*.*")),
                           title="Choose a file."
                           )
    print(name)

    # Using try in case user types in unknown file or closes without choosing a file.
    try:
        with open(name, 'r') as UseFile:
            print(UseFile.read())
    except Exception as e:
        logging.exception(e)


root = Tk()
root.title("Translator")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

text = StringVar()
lang = StringVar()
result = StringVar()


text_box = Text(mainframe, width=40, height=8)
text_box.grid(column=2, row=2, sticky=(W, E))


text_entry = ttk.Entry(mainframe, width=50, textvariable=text)
text_entry.grid(column=2, row=1, sticky=(W, E))

var = StringVar()
option = ttk.OptionMenu(mainframe, var, "vi", "ru", "ja", "zh-TW", "fr", "de", "ko")
option.grid(column=4, row=1, sticky=(W, E))

ttk.Button(mainframe, text="Translate", command=translate_text).grid(column=4, row=3, sticky=W)
ttk.Button(mainframe, text="Open File", command=open_file).grid(column=3, row=3, sticky=W)

ttk.Label(mainframe, textvariable=result).grid(column=2, row=3, sticky=(W, E))
ttk.Label(mainframe, text="Text:").grid(column=1, row=1, sticky=W)
ttk.Label(mainframe, text="Language:").grid(column=3, row=1, sticky=W)


for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)

text_entry.focus()
root.bind('<Return>', translate_text)
root.geometry("700x480")
load = Image.open("google.png")
render = ImageTk.PhotoImage(load)
img = Label(image=render)
img.image = render
img.place(x=50, y=270)

root.mainloop()
